#!/usr/bin/env python3
import imaplib
import shlex
import json
import argparse
from pushbullet import Pushbullet #pip3 install pushbullet.py

if __name__=="__main__":

	parser = argparse.ArgumentParser()
	parser.add_argument("--config", type=str,
					help="config filename", default="config.json")
	args = parser.parse_args()

	config = json.load(open(args.config, "rb"))
	quotaWarnings = []

	for email in config['email']:
		if not email["enable"]:
			continue

		#Inspired by https://askubuntu.com/a/39687
		if email["ssl"]:
			IMAP4_CON = imaplib.IMAP4_SSL
		else:
			IMAP4_CON = imaplib.IMAP4
		try:
			conn = IMAP4_CON(email['host'])			
			conn.login(email['username'], email['password'])
		except IMAP4_CON.error as err:
			quotaWarnings.append((email['account'], "error", str(err)))
			continue

		#List quotas
		#status, roots = conn.getquotaroot('INBOX')
		#if status == "OK":
		#	for root in roots:
		#		print (root)

		status, quotastr = conn.getquota(email['quota'])
		if status == "OK" and len(quotastr) > 0:
			quotasplit = shlex.split(quotastr[0].decode('UTF-8'))
			used = int(quotasplit[2])
			total = int(quotasplit[3].strip("()"))
			usage = float(used)/total
			print (email['account'], usage)
			if usage > email['warn_level']:
				quotaWarnings.append((email['account'], "warn", usage))
		else:
			quotaWarnings.append((email['account'], "error", quotastr))

	if len(quotaWarnings) > 0 and len(config['pushbullet_key'])>0:
		pb = Pushbullet(config['pushbullet_key'])
		body = []
		for qw in quotaWarnings:
			if qw[1] == "warn":
				body.append("Email account {} is using {:.00f}% of quota\n".format(qw[0], qw[2]*100.0))
			elif qw[1] == "error":
				body.append("Email account {}: {}\n".format(qw[0], qw[2]))

		print (body)
		pushResult = pb.push_note("Email quota warning", "".join(body))
		#print (pushResult)

